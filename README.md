# vue-modal

![vue2](https://img.shields.io/badge/vue-2.6.x-brightgreen.svg)
![free](https://img.shields.io/badge/open-source-red.svg)
![ver](https://img.shields.io/badge/ver-0.1.0-blue.svg)

The `v-modal` component inform users about a specific task and may contain critical information, 
require decisions, or involve multiple tasks. Use dialogs sparingly because they are interruptive.

## Table of Contents
- **[Demo](#demo)**
- **[Installation](#install)**
- **[Usage](#usage)**
    - [Basic example](#basic-example)
    - [Props](#props)
    - [Slots](#slots)
    - [Events](#events)
- **[License](#license)**


## Installation

This project uses `node` and `yarn`. Go check them out if you don't have them locally installed.

#### Project setup
```
$ yarn install
```

#### Compiles and hot-reloads for development
```
$ yarn serve
```

#### Compiles and minifies for production
```
$ yarn build
```

#### Run your unit tests
```
$ yarn test:unit
```

#### Lints and fixes files
```
$ yarn lint
```

---
## License

[![MIT](https://img.shields.io/badge/license-MIT-green.svg)](http://opensource.org/licenses/MIT)

© Foxy Dev