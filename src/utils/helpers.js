export const getSlot = (vm, name = 'default', data, optional = false) => {
    if (vm.$scopedSlots[name])
        return vm.$scopedSlots[name](data instanceof Function ? data() : data);

    if (vm.$slots[name] && (!data || optional))
        return vm.$slots[name];

    return undefined;
};

export const convertToUnit = (str, unit = 'px') => {
    if (str == null || str === '') {
        return undefined;
    } else if (isNaN(+str)) {
        return String(str);
    } else return `${Number(str)}${unit}`;
}

/**
 * Returns:
 *  - 'scoped' for old style scoped slots (`<template slot="default" slot-scope="data">`) or bound v-slot (`#default="data"`)
 *  - 'v-slot' for unbound v-slot (`#default`) - only if the third param is true, otherwise counts as scoped
 */
export const getSlotType = (vm, name, split) => {
    if (vm.$slots[name] && vm.$scopedSlots[name] && vm.$scopedSlots[name].name)
        return split ? 'v-slot' : 'scoped';
}

export const debounce = (callback, delay) => {
    let timeoutId = 0;
    return (...args) => {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(() => callback(...args), delay);
    };
}

export const addOnceEventListener = (el, eventName, cb, options = false) => {
    const once = (event) => {
        cb(event);
        el.removeEventListener(eventName, once, options);
    };

    el.addEventListener(eventName, once, options);
}

export const getZIndex = (el) => {
    if (!el || el.nodeType !== Node.ELEMENT_NODE) return 0;

    const index = +window.getComputedStyle(el).getPropertyValue('z-index');
    return index || getZIndex(el.parentNode);
};

export const addPassiveEventListener = (el, event, cb, options) => {
    let passiveSupported = false;
    try {
        if (typeof window !== 'undefined') {
            const testListenerOpts = Object.defineProperty && Object.defineProperty({}, 'passive', {
                get: () => {
                    passiveSupported = true;

                    return passiveSupported;
                },
            });
            window.addEventListener('testListener', testListenerOpts, testListenerOpts);
            window.removeEventListener('testListener', testListenerOpts, testListenerOpts);
        }
    }
    catch (e) {
        console.warn(e);
    }

    el.addEventListener(event, cb, passiveSupported ? options : false);
}

export const isCssColor = (color) => !!color && !!color.match(/^(#|var\(--|(rgb|hsl)a?\()/);

export const keyCodes = Object.freeze({
    enter: 13,
    tab: 9,
    delete: 46,
    esc: 27,
    space: 32,
    up: 38,
    down: 40,
    left: 37,
    right: 39,
    end: 35,
    home: 36,
    del: 46,
    backspace: 8,
    insert: 45,
    pageup: 33,
    pagedown: 34,
    shift: 16,
})

export const getNestedValue = (obj, path, fallback) => {
    const last = path.length - 1;
    if (last < 0) return obj === undefined ? fallback : obj;

    for (let i = 0; i < last; i++) {
        if (obj == null) return fallback;

        obj = obj[path[i]];
    }

    if (obj == null) return fallback;

    return obj[path[last]] === undefined ? fallback : obj[path[last]];
}

export const getObjectValueByPath = (obj, path, fallback) => {
    if (obj == null || !path || typeof path !== 'string') return fallback;

    if (obj[path] !== undefined) return obj[path];

    path = path.replace(/\[(\w+)\]/g, '.$1');
    path = path.replace(/^\./, '');

    return getNestedValue(obj, path.split('.'), fallback);
}

/**
 * Returns:
 *  - 'null' if the node is not attached to the DOM
 *  - the root node (HTMLDocument | ShadowRoot) otherwise
 */
export const attachedRoot = (node) => {
    if (typeof node.getRootNode !== 'function') {
        while (node.parentNode)
            node = node.parentNode;

        return node !== document ?  null : document;
    }

    const root = node.getRootNode();
    return (root !== document && root.getRootNode({ composed: true }) !== document) ?  null : root;
}

export default {
    getSlot,
    keyCodes,
    convertToUnit,
    addOnceEventListener,
    addPassiveEventListener,
    isCssColor,
    getObjectValueByPath,
    getNestedValue,
    attachedRoot,
};