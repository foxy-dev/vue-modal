import VModal from '@/components/v-modal';
import { modalName } from "@/constants";

export default {
  install (Vue, options) {
    Vue.component('v-modal', { ...VModal, props: {
        ...VModal.props,
        componentName: { default: options.componentName || modalName },
      }});
  }
};

export { VModal };
