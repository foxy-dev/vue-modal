import Vue from 'vue';
import App from './App.vue';
import VModal from '@/plugins/v-modal';

Vue.use(VModal, { componentName: 'v-modal' });

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
