const  searchChildren = (children) => {
    const results = [];
    for (let index = 0; index < children.length; index++) {
        const child = children[index];
        if (child.isActive && child.isDependent) {
            results.push(child);
        }
        else {
            results.push(...searchChildren(child.$children));
        }
    }
    return results;
}

export default {
    name: 'dependent',

    data () {
        return {
            closeDependents: true,
            isActive: false,
            isDependent: true,
        };
    },

    watch: {
        isActive: 'isActiveDependentsHandler',
    },

    methods: {
        isActiveDependentsHandler (val) {
            if (val) return

            const openDependents = this.getOpenDependents();
            openDependents.forEach((index) => { openDependents[index].isActive = false });
        },

        getOpenDependents() {
            return this.closeDependents ? searchChildren(this.$children) : [];
        },

        getOpenDependentElements() {
            const openDependents = this.getOpenDependents();

            return openDependents.map((el) => el.push(el.getClickableDependentElements()));
        },

        getClickableDependentElements() {
            const result = [this.$el];

            if (this.$refs.content) result.push(this.$refs.content);
            if (this.overlay) result.push(this.overlay.$el);

            result.push(...this.getOpenDependentElements());

            return result;
        },
    },
};
