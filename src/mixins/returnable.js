export default {
    name: 'returnable',

    props: {
        returnValue: {
            required: false,
        },
    },

    data () {
        return {
            isActive: false,
            originalValue: null,
        }
    },

    watch: {
        isActive: 'handleReturnable',
    },

    methods: {
        handleReturnable (val) {
            if (val) {
                this.originalValue = this.returnValue;
            } else {
                this.$emit('update:return-value', this.originalValue);
            }
        },

        save (value) {
            this.originalValue = value;
            setTimeout(() => { this.isActive = false;});
        },
    },
}
