// Mixins
import Bootable from './bootable';

// Utilities
import { getObjectValueByPath } from '../utils/helpers';

const validateAttachTarget = (val) => {
    const type = typeof val;
    if (type === 'boolean' || type === 'string') return true;

    return val.nodeType === Node.ELEMENT_NODE;
}
export default {
    name: 'detachable',

    mixins: [Bootable],

    props: {
        attach: {
            default: false,
            validator: validateAttachTarget,
        },

        contentClass: {
            type: String,
            default: '',
        },
    },

    data () {
        return {
            activatorNode: null,
            hasDetached: false,
        }
    },

    watch: {
        attach () {
            this.hasDetached = false;
            this.initDetach();
        },

        hasContent () {
            this.$nextTick(this.initDetach);
        },
    },

    beforeMount() {
        this.$nextTick(() => {
            if (this.activatorNode) {
                const activator = Array.isArray(this.activatorNode) ? this.activatorNode : [this.activatorNode];

                activator.forEach(node => {
                    if (!node.elm || !this.$el.parentNode) return;

                    const target = this.$el === this.$el.parentNode.firstChild ? this.$el : this.$el.nextSibling;
                    this.$el.parentNode.insertBefore(node.elm, target);
                });
            }
        });
    },

    mounted() {
        this.hasContent && this.initDetach();
    },

    deactivated() {
        this.isActive = false;
    },

    methods: {
        getScopeIdAttrs () {
            const scopeId = getObjectValueByPath(this.$vnode, 'context.$options._scopeId');

            return scopeId && {
                [scopeId]: '',
            };
        },

        initDetach() {
            if (this._isDestroyed || !this.$refs.content || this.hasDetached ||
                this.attach === '' || this.attach === true || this.attach === 'attach'
            ) return;

            let target;
            if (this.attach === false) {
                target = document.querySelector('#app');
            } else if (typeof this.attach === 'string') {
                target = document.querySelector(this.attach);
            } else {
                target = this.attach;
            }

            if (!target) return;

            target.appendChild(this.$refs.content);
            this.hasDetached = true;
        },
    },
};
