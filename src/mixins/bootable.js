// Utils
export default {
    name: 'bootable',

    props: {
        eager: Boolean,
    },

    data () {
        return {
            isBooted: false,
        }
    },

    computed: {
        hasContent() {
            return this.isBooted || this.eager || this.isActive;
        },
    },

    watch: {
        isActive() {
            this.isBooted = true;
        },
    },

    methods: {
        showLazyContent(content) {
            return (this.hasContent && content) ? content() : [this.$createElement()];
        },
    },
};