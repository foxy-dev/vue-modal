// Mixins
import Delayable from './delayable';
import Toggleable from './toggleable';

import { getSlot } from '../utils/helpers';

export default {
    name: 'activatable',

    mixins: [Toggleable, Delayable],

    props: {
        activator: {
            default: null,
            validator: (val) => {
                return ['string', 'object'].includes(typeof val);
            },
        },

        disabled: Boolean,

        internalActivator: Boolean,

        openOnHover: Boolean,

        openOnFocus: Boolean,
    },

    data () {
        return {
            activatorElement: null,
            activatorNode: [],
            events: ['click', 'mouseenter', 'mouseleave', 'focus'],
            listeners: {},
        }
    },

    watch: {
        activator: 'resetActivator',
        openOnFocus: 'resetActivator',
        openOnHover: 'resetActivator',
    },

    mounted () {
        this.addActivatorEvents();
    },

    beforeDestroy() {
        this.removeActivatorEvents();
    },

    methods: {
        addActivatorEvents () {
            if (!this.activator || this.disabled || !this.getActivator()) return;

            this.listeners = this.genActivatorListeners();
            const keys = Object.keys(this.listeners);

            for (const key of keys) {
                this.getActivator().addEventListener(key, this.listeners[key]);
            }
        },

        genActivator() {
            const node = getSlot(this, 'activator', Object.assign(this.getValueProxy(), {
                on: this.genActivatorListeners(),
                attrs: this.genActivatorAttributes(),
            })) || [];
            this.activatorNode = node;

            return node;
        },

        genActivatorAttributes() {
            return {
                role: 'button',
                'aria-haspopup': true,
                'aria-expanded': String(this.isActive),
            };
        },

        genActivatorListeners() {
            if (this.disabled) return {};

            const listeners = {};

            if (this.openOnHover) {
                listeners.mouseenter = (e) => {
                    this.getActivator(e);
                    this.runDelay('open');
                };

                listeners.mouseleave = (e) => {
                    this.getActivator(e);
                    this.runDelay('close');
                };
            } else {
                listeners.click = (e) => {
                    const activator = this.getActivator(e);
                    if (activator) activator.focus();
                    e.stopPropagation();
                    this.isActive = !this.isActive;
                };
            }

            if (this.openOnFocus) {
                listeners.focus = (e) => {
                    this.getActivator(e);
                    e.stopPropagation();
                    this.isActive = !this.isActive;
                };
            }

            return listeners;
        },

        getActivator (e) {
            if (this.activatorElement) return this.activatorElement;

            let activator = null;

            if (this.activator) {
                const target = this.internalActivator ? this.$el : document;
                if (typeof this.activator === 'string') {
                    activator = target.querySelector(this.activator);
                } else if (this.activator.$el) {
                    activator = this.activator.$el;
                } else {
                    activator = this.activator;
                }
            } else if (this.activatorNode.length === 1 || (this.activatorNode.length && !e)) {
                const vm = this.activatorNode[0].componentInstance;
                if (vm
                    && vm.$options.mixins
                    && vm.$options.mixins.some((m) =>
                        m.options && ['activatable', 'menuable'].includes(m.options.name))
                ) {
                    activator = vm.getActivator();
                } else {
                    activator = this.activatorNode[0].elm;
                }
            } else if (e) {
                activator = (e.currentTarget || e.target);
            }
            this.activatorElement = activator;

            return this.activatorElement;
        },

        getContentSlot () {
            return getSlot(this, 'default', this.getValueProxy(), true)
        },

        getValueProxy () {
            const self = this;

            return {
                get value () {
                    return self.isActive
                },

                set value (isActive) {
                    self.isActive = isActive
                },
            }
        },

        removeActivatorEvents() {
            if (!this.activator || !this.activatorElement) return;

            const keys = Object.keys(this.listeners);
            for (const key of keys) {
                this.activatorElement.removeEventListener(key, this.listeners[key]);
            }

            this.listeners = {};
        },

        resetActivator() {
            this.removeActivatorEvents();
            this.activatorElement = null;
            this.getActivator();
            this.addActivatorEvents();
        },
    },
};
