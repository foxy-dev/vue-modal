import { getZIndex } from '../utils/helpers';

export default {
    name: 'stackable',

    data() {
        return {
            stackElement: null,
            stackExclude: null,
            stackMinZIndex: 0,
            isActive: false,
        };
    },

    computed: {
        activeZIndex() {
            if (typeof window === 'undefined') return 0;

            const content = this.stackElement || this.$refs.content;


            const index = !this.isActive
                ? getZIndex(content)
                : this.getMaxZIndex(this.stackExclude || [content]) + 2;

            if (index == null) return index;

            return parseInt(index);
        },
    },

    methods: {
        getMaxZIndex(exclude = []) {
            const base = this.$el;
            const zis = [this.stackMinZIndex, getZIndex(base)];

            const activeElements = [
                ...document.getElementsByClassName('v-modal__content--active'),
            ];

            activeElements.forEach(el => {
               exclude.includes(el) || zis.push(getZIndex(el))
            });

            for (let index = 0; index < activeElements.length; index++) {
                if (!exclude.includes(activeElements[index])) {
                    zis.push(getZIndex(activeElements[index]));
                }
            }

            return Math.max(...zis);
        },
    },
};
