// Components
import VOverlay from '../components/v-overlay';

// Utilities
import { keyCodes, addOnceEventListener, addPassiveEventListener, getZIndex, } from '../utils/helpers';
import Vue from 'vue';

export default Vue.extend({
    name: 'overlayable',

    props: {
        hideOverlay: Boolean,

        overlayColor: String,

        overlayOpacity: [Number, String],
    },

    data () {
        return {
            animationFrame: 0,
            overlay: null,
        };
    },

    watch: {
        hideOverlay: 'handleActiveOverlay',
    },

    beforeDestroy() {
        this.removeOverlay();
    },

    methods: {
        handleActiveOverlay (val) {
            if (!this.isActive) return;

            val ? this.removeOverlay() : this.genOverlay();
        },

        createOverlay() {
            const overlay = new VOverlay({
                propsData: {
                    absolute: this.absolute,
                    value: false,
                    color: this.overlayColor,
                    opacity: this.overlayOpacity,
                },
            });
            overlay.$mount();

            const parent = this.absolute
                ? this.$el.parentNode
                : document.querySelector('#app');
            parent && parent.insertBefore(overlay.$el, parent.firstChild);
            this.overlay = overlay;
        },

        genOverlay() {
            this.hideScroll();
            if (this.hideOverlay) return;

            if (!this.overlay) this.createOverlay();

            this.animationFrame = requestAnimationFrame(() => {
                if (!this.overlay) return;

                if (this.activeZIndex !== undefined) this.overlay.zIndex = String(this.activeZIndex - 1);

                else if (this.$el) this.overlay.zIndex = getZIndex(this.$el);

                this.overlay.value = true;
            });

            return true;
        },

        /** removeOverlay(false) will not restore the scollbar afterwards */
        removeOverlay (showScroll = true) {
            if (this.overlay) {
                addOnceEventListener(this.overlay.$el, 'transitionend', () => {
                    const isOverlay = !this.overlay
                        || !this.overlay.$el
                        || !this.overlay.$el.parentNode
                        || this.overlay.value;

                    if (isOverlay) return;

                    this.overlay.$el.parentNode.removeChild(this.overlay.$el);
                    this.overlay.$destroy();
                    this.overlay = null;
                });
                cancelAnimationFrame(this.animationFrame);
                this.overlay.value = false;
            }

            showScroll && this.showScroll();
        },

        scrollListener (e) {
            if (e.type === 'keydown') {
                if (['INPUT', 'TEXTAREA', 'SELECT'].includes(e.target.tagName) || e.target.isContentEditable) return;

                const up = [keyCodes.up, keyCodes.pageup];
                const down = [keyCodes.down, keyCodes.pagedown];

                if (up.includes(e.keyCode)) e.deltaY = -1;

                if (down.includes(e.keyCode)) e.deltaY = 1;
                else return;
            }

            if (e.target === this.overlay || (e.type !== 'keydown' && e.target === document.body) || this.checkPath(e))
                e.preventDefault();
        },

        hasScrollbar (el) {
            if (!el || el.nodeType !== Node.ELEMENT_NODE) return false;
            const style = window.getComputedStyle(el);

            return ['auto', 'scroll'].includes(style.overflowY) && el.scrollHeight > el.clientHeight;
        },

        shouldScroll (el, delta) {
            if (el.scrollTop === 0 && delta < 0) return true;

            return el.scrollTop + el.clientHeight === el.scrollHeight && delta > 0;
        },

        isInside (el, parent) {
            if (el === parent) return true;

            return el === null || el === document.body ? false : this.isInside(el.parentNode, parent);
        },

        checkPath (e) {
            const path = e.path;
            const delta = e.deltaY;

            if (e.type === 'keydown' && path[0] === document.body) {
                const modal = this.$refs.modal;
                const selected = window.getSelection().anchorNode;

                if (modal && this.hasScrollbar(modal) && this.isInside(selected, modal)) {
                    return this.shouldScroll(modal, delta);
                }

                return true;
            }

            const scrollEl = path.find(this.hasScrollbar);
            return scrollEl ? this.shouldScroll(scrollEl, delta) : true;
        },

        hideScroll () {
            if ( window.innerWidth < 800 ) {
                document.documentElement.classList.add('overflow-y-hidden');
            } else {
                addPassiveEventListener(window, 'wheel', this.scrollListener, { passive: false });
                window.addEventListener('keydown', this.scrollListener);
            }
        },

        showScroll () {
            document.documentElement.classList.remove('overflow-y-hidden');
            window.removeEventListener('wheel', this.scrollListener);
            window.removeEventListener('keydown', this.scrollListener);
        },
    },
});
