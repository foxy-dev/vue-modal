export default {
    name: 'delayable',

    props: {
        openDelay: {
            type: [Number, String],
            default: 0,
        },

        closeDelay: {
            type: [Number, String],
            default: 0,
        },
    },

    data () {
        return {
            openTimeout: undefined,
            closeTimeout: undefined,
        }
    },

    methods: {
        /**
         * Clear any pending delay timers from executing
         */
        clearDelay() {
            clearTimeout(this.openTimeout);
            clearTimeout(this.closeTimeout);
        },

        /**
         * Runs callback after a specified delay
         */
        runDelay(type, cb) {
            this.clearDelay();
            const delay = parseInt(this[`${type}Delay`], 10);
            this[`${type}Timeout`] = setTimeout(cb || (() => {
                this.isActive = { open: true, close: false }[type];
            }), delay);
        },
    },
};