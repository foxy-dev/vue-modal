export const factory = (prop = 'value', event = 'input') => {
    return {
        name: 'toggleable',
        model: { prop, event },
        props: {
            [prop]: { required: false },
        },

        data() {
            return {
                isActive: !!this[prop],
            };
        },

        watch: {
            [prop] (val) { this.isActive = !!val;},
            isActive (val) {
                !!val !== this[prop] && this.$emit(event, val);
            },
        },
    };
}
/* eslinst-disable-next-line @typescript-eslint/no-redeclare */
const Toggleable = factory();
export default Toggleable;