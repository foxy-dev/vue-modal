export const modalName = 'v-modal';
const states = ['active', 'persistent', 'fullscreen', 'scrollable', 'animated'];

export const modalConfig = (name = modalName) => ({
    name,
    container: `${name}__container`,
    states: states.reduce((all, curr) => ({...all, [curr]:`${name}--${curr}`})),
});

export const colorTypes = ['primary', 'secondary', 'success', 'danger', 'warning', 'info' ];
export const sizes = ['sm', 'md', 'lg', 'xl'];

export default { modalName,  modalConfig, colorTypes, sizes};
