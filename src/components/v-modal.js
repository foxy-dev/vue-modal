// Styles
import '../styles/v-modal.scss';

// Mixins
import Activatable from '../mixins/activatable';
import Dependent from '../mixins/dependent';
import Detachable from '../mixins/detachable'
import Overlayable from '../mixins/overlayable';
import Returnable from '../mixins/returnable';
import Stackable from '../mixins/stackable';
import Toggleable from '../mixins/toggleable';

// Directives
import ClickOutside from '../directives/click-outside';

// Utils
import { convertToUnit, keyCodes } from '../utils/helpers';
import { modalName, modalConfig } from '../constants';

const focusableElements = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
const currentMixins = [
    Activatable,
    Dependent,
    Detachable,
    Overlayable,
    Returnable,
    Stackable,
    Toggleable
];

export default {
    name: modalName,

    mixins: currentMixins,

    directives: { ClickOutside },

    props: {
        disabled: Boolean,

        fullscreen: Boolean,

        maxWidth: {
            type: [String, Number],
            default: 'none',
        },

        maxHeight: {
            type: [String, Number],
            default: 'none',
        },

        origin: {
            type: String,
            default: 'center center',
        },

        noClickAnimation: Boolean,

        persistent: Boolean,

        retainFocus: {
            type: Boolean,
            default: true,
        },

        scrollable: Boolean,

        transition: {
            type: [String, Boolean],
            default: 'modal-transition',
        },

        width: {
            type: [String, Number],
            default: 'auto',
        },

        height: {
            type: [String, Number],
            default: 'auto',
        },

        componentName: {
            type: String,
            default: modalName,
        },

        hasCloseBtn: {
            type: Boolean,
            default: true,
        },
    },

    data () {
        return {
            activatedBy: null,
            animate: false,
            animateTimeout: -1,
            isActive: !!this.value,
            stackMinZIndex: 200,
            previousActiveElement: null,
        };
    },

    computed: {
        config () {
            return modalConfig(this.componentName);
        },

        classes () {
            return {
                [(`${this.config.name} ${this.contentClass}`).trim()]: true,
                [this.config.states.active]: this.isActive,
                [this.config.states.persistent]: this.persistent,
                [this.config.states.fullscreen]: this.fullscreen,
                [this.config.states.scrollable]: this.scrollable,
                [this.config.states.animate]: this.animate,
            };
        },

        contentClasses () {
            return {
                [`${this.config.name}__content`]: true,
                [`${this.config.name}__content--active`]: this.isActive,
            };
        },
    },

    watch: {
        isActive: 'handleActiveState',
        fullscreen: 'handleFullScreenState',
    },

    beforeMount () {
        this.$nextTick(() => {
            this.isBooted = this.isActive;
            this.isActive && this.openModal();
        });
    },

    beforeDestroy () {
        if (typeof window !== 'undefined') this.unbind();
    },

    methods: {
        /**
         * @description handler for active state prop
         */
        handleActiveState (val) {
            if (val) {
                this.openModal();
                this.hideScroll();
            } else {
                this.removeOverlay();
                this.unbind();
                this.previousActiveElement && this.previousActiveElement.focus();
            }
        },

        /**
         * @description handler for fullscreen prop
         */
        handleFullScreenState (val) {
            if (!this.isActive) return;

            if (val) {
                this.hideScroll();
                this.removeOverlay(false);
            } else {
                this.showScroll();
                this.genOverlay();
            }
        },

        /**
         * @description Needed for when clicking very fast outside of the modal
         */
        animateClick () {
            this.animate = false;
            this.$nextTick(() => {
                this.animate = true;
                window.clearTimeout(this.animateTimeout);
                this.animateTimeout = window.setTimeout(() => (this.animate = false), 150);
            });
        },

        /**
         * @description Ignore the click if the modal is closed or destroyed,
         * @returns {boolean}
         */
        closeConditional (e) {
            const target = e.target;
            const hasOverlay = (this.overlay && target && !this.overlay.$el.contains(target));
            const destroyedOrDisactivated = this._isDestroyed || !this.isActive || this.$refs.content.contains(target);
            const isHigher = this.activeZIndex >= this.getMaxZIndex();

            return !(destroyedOrDisactivated || hasOverlay) && isHigher;
        },

        /**
         * @description - hide scroll on modal is open.
         */
        hideScroll () {
            this.fullscreen
                ? document.documentElement.classList.add('overflow-y-hidden')
                : Overlayable.options.methods.hideScroll.call(this)
        },

        /**
         * @description - open modal
         * Double nextTick to wait for lazy content to be generated
         */
        openModal () {
            !this.fullscreen && !this.hideOverlay && this.genOverlay();

            this.$nextTick(() => {
                this.$nextTick(() => {
                    this.previousActiveElement = document.activeElement;
                    this.$refs.content.focus();
                    this.bind();
                });
            });
        },

        /**
         * @description - add focus eventListener
         */
        bind () {
            window.addEventListener('focusin', this.onFocusin);
        },

        /**
         * @description - remove focus event listener
         */
        unbind () {
            window.removeEventListener('focusin', this.onFocusin);
        },

        /**
         * @description - hide on click outside
         */
        onClickOutside (e) {
            this.$emit('click:outside', e);
            if (this.persistent) this.noClickAnimation || this.animateClick();
            this.isActive = false;
        },

        /**
         * @description - close by ESC button
         */
        onKeydown (e) {
            if (e.keyCode === keyCodes.esc && !this.getOpenDependents().length) {
                if (!this.persistent) this.isActive = false;
                if (!this.noClickAnimation) this.animateClick();
            }
            this.$emit('keydown', e);
        },

        /**
         * On focus change, wrap focus to stay inside the modal
         * @description focus on focusable element;
         */
        onFocusin (e) {
            if (!e || !this.retainFocus) return;

            const target = e.target;
            const isHigher = this.activeZIndex >= this.getMaxZIndex();
            const notDocAndChilds = !!target
                && ![document, this.$refs.content].includes(target)
                && !this.$refs.content.contains(target);

            if ( notDocAndChilds && isHigher ) {
                const focusable = this.$refs.content.querySelectorAll(focusableElements);
                const el = [...focusable].find(el => !el.hasAttribute('disabled'));
                el && el.focus();
            }
        },

        /**
         * @description - create content for modal
         * @returns {VNode|*}
         */
        genContent () {
            return this.showLazyContent(() => [
                this.$createElement('div', {
                    props: {
                        root: true,
                    },
                    class: this.contentClasses,
                    attrs: {
                        role: 'document',
                        tabindex: this.isActive ? 0 : undefined,
                        ...this.getScopeIdAttrs(),
                    },
                    on: { keydown: this.onKeydown },
                    style: { zIndex: this.activeZIndex },
                    ref: 'content',
                }, [this.genTransition()]),
            ]);
        },

        /**
         * @description - wrapper for transitions
         * @returns {VNode|*}
         */
        genTransition() {
            const content = this.genInnerContent();
            if (!this.transition) return content;

            return this.$createElement('transition', {
                props: { name: this.transition, origin: 'center center', appear: true,},
            }, [content]);
        },

        /**
         * @description - create close btn (x)
         * @returns {VNode}
         * */
        genCloseBtn() {
            return this.$createElement('button', {
                class: `${this.componentName}__close-btn`,
                attrs: {
                    title: 'Close',
                    ariaLabel: 'Close dialog',
                },
                on: {
                  click: (event) => {
                      event.stopPropagation();
                      this.$emit('close');
                      this.isActive = false;
                  }
                },
            })
        },

        /**
         * @description - generate content element for modal
         * @returns {VNode}
         */
        genInnerContent() {
            const data = {
                class: this.classes,
                ref: 'modal',
                directives: [
                    {
                        name: 'click-outside',
                        value: {
                            handler: this.onClickOutside,
                            closeConditional: this.closeConditional,
                            include: this.getOpenDependentElements,
                        },
                    },
                    { name: 'show', value: this.isActive },
                ],
                style: {
                    transformOrigin: 'center center',
                },
            };

            const elements = [this.getContentSlot()];

            if (!this.fullscreen) {
                data.style = {
                    ...data.style,
                    maxWidth: this.maxWidth === 'none' ? undefined : convertToUnit(this.maxWidth),
                    maxHeight: this.maxHeight === 'none' ? undefined : convertToUnit(this.maxHeight),
                    height: this.height === 'auto' ? undefined : convertToUnit(this.height),
                    width: this.width === 'auto' ? undefined : convertToUnit(this.width),
                }

                if (this.hasCloseBtn && !this.persistent) {
                    elements.push(this.genCloseBtn());
                }
            }

            return this.$createElement('div', data, elements);
        },
    },

    render (h) {
        return h('div', {
            staticClass: this.config.container,
            class: {
                [`${this.config.container}--attached`]:
                    this.attach === '' ||
                    this.attach === true ||
                    this.attach === 'attach',
            },
            attrs: { role: 'dialog' },
        }, [
            this.genActivator(),
            this.genContent(),
        ])
    },
};
