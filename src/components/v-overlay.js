// Styles
import '../styles/v-overlay.scss';

// Mixins
import { isCssColor } from '../utils/helpers';
import Toggleable from '../mixins/toggleable';
import Vue from 'vue';

/* @vue/component */
export default Vue.extend({
    name: 'v-overlay',

    mixins: [Toggleable],

    props: {
        absolute: Boolean,

        color: {
            type: String,
            default: '#212121',
        },

        opacity: {
            type: [Number, String],
            default: 0.26,
        },

        value: {
            default: true,
        },

        zIndex: {
            type: [Number, String],
            default: 5,
        },
    },

    computed: {
        layout () {
            return this.$createElement('div', this.setBackgroundColor(this.color, {
                staticClass: 'v-overlay__scrim',
                style: {
                    opacity: this.isActive ? this.opacity : 0,
                },
            }));
        },
    },

    methods: {
        setBackgroundColor(color, data = {}) {
            if (isCssColor(color)) {
                data.style = {
                  ...data.style,
                  'background-color': `${color}`,
                  'border-color': `${color}`
                };
            } else if (color) {
                data.class = { ...data.class, [color]: true };
            }

            return data;
        },

        genContent() {
            return this.$createElement('div', { staticClass: 'v-overlay__content' }, this.$slots.default);
        },
    },

    render(createElement) {
        const children = [this.layout];

        if (this.isActive) children.push(this.genContent());

        return createElement('div', {
            staticClass: 'v-overlay',
            class: {
                'v-overlay--absolute': this.absolute,
                'v-overlay--active': this.isActive
            },
            style: {
                zIndex: this.zIndex,
            },
        }, children);
    },
});
