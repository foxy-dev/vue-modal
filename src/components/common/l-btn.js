import '@/styles/l-btn.scss';

import Vue from 'vue';
import { colorTypes, sizes } from '@/constants';

const colorNames = (propsKeys, name = 'button--') => colorTypes.reduce((all, type) =>
    ({...all, [name + type]: propsKeys.includes(type)}), {});

const sizeNames = (context) => sizes.reduce((all, type) =>
    ({...all, [`button--${type}`]: context.props.size === type}), {});

const getChildrenTextContent = (context) => context.children
    ? context.children.map((node) => node.children ? getChildrenTextContent(node.children) : node.text).join('')
    : context.props.label || 'Button';

export default Vue.extend({
    name: 'l-btn',
    functional: true,
    render (createElement, context) {
        const propsKeys = Object.keys(context.props);
        const textContent = getChildrenTextContent(context);
        const title = createElement('span', {staticClass: 'button__text'}, context.children || textContent);
        const loader = createElement('div', {staticClass: 'button__loader',});

        return createElement('button',
            {
                ...context.data,
                staticClass: `button`,
                class: {
                    ...colorNames(propsKeys),
                    ...sizeNames(context),
                    [context.data.staticClass]: context.data.staticClass,
                    'button--outlined': context.props.outlined,
                    'button--loading': context.props.loading,
                },
                attrs: {
                    ...context.attrs,
                    'aria-label': textContent,
                    title: textContent,
                    tabIndex: 0,
                    disabled: !!context.props.disabled,
                },
            }, [title, context.props.loading && loader]);
    },
});
