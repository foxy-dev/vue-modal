// l-btn.stories.js
import LBtn from './l-btn';
import { action } from '@storybook/addon-actions';

import { sizes, colorTypes } from "@/constants";

const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);

export default {
  component: LBtn,
  title: 'Design System/Common/Button',
  parameters:{
    layout:'centered',
  },
  argTypes: {
    label: {
      name: ':label',
      type: {
        name: 'string',
        required: false,
      },
      defaultValue: 'Hello',
      description: 'Button\'s label',
      table: {
        type: {
          summary: 'string'
        },
        defaultValue: {
          summary: 'Base Button'
        },
      },
      control: {
        type: 'input'
      }
    },
    size: {
      name: ':size',
      type: {
        name: 'string',
        required: false,
      },
      defaultValue: 'md',
      description: 'Button\'s size',
      table: {
        type: {
          summary: 'string',
        },
        defaultValue: {
          summary: 'md',
        },
        control: {
          type: 'select',
        },
      },

      control: {
        type: 'select',
        options: sizes,
      },

    },
    outlined: {
      name: ':outlined',
      type: {
        name: 'boolean',
        required: false,
      },
      defaultValue: false,
      description: 'Outline button style',
      table: {
        type: {
          summary: 'boolean',
        },
        defaultValue: {
          summary: 'md',
        },
        control: {
          type: 'checkbox',
        },
      },
    },
    disabled: {
      name: ':disabled',
      type: {
        name: 'boolean',
        required: false,
      },
      defaultValue: false,
      description: 'Disabled button state',
      table: {
        type: {
          summary: 'boolean',
        },
        defaultValue: {
          summary: false,
        },
        control: {
          type: 'checkbox',
        },
      },
    },
  }

};

const Template = (args, { argTypes }) => ({
  components: { LBtn },
  props: Object.keys(argTypes),
  template: '<l-btn v-bind="$props" v-on="$props" @click="onClick"/>',
  methods: {
    onClick: action('clicked'),
  }
});

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: 'Base Button',
};

export const Secondary = Template.bind({});
Secondary.args = {
  secondary: true,
  label: 'Secondary Button',
};

export const Success = Template.bind({});
Success.args = {
  success: true,
  label: 'Success Button',
};

export const Danger = Template.bind({});
Danger.args = {
  danger: true,
  label: 'Danger Button',
};

export const Warning = Template.bind({});
Warning.args = {
  warning: true,
  label: 'Warning Button',
};

export const Info = Template.bind({});
Info.args = {
  info: true,
  label: 'Info Button',
};
